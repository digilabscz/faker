<?php declare(strict_types=1);

namespace Digilabscz\Faker;

use Exception;
use Nette\Utils\Strings;

final class Generator
{
    public const string DictionaryWords = 'Words';
    public const string DictionaryFirstNames = 'FirstNames';
    public const string DictionaryLastNames = 'LastNames';

    private const array AlphabetCharacters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    private const array SpecialCharacters = ['*', '/', '-', '_', '.', ',', '&', '^', '%', '$', '#', '@', '+'];

    /**
     * @var array
     */
    private static array $dictionaries = [];
    private static array $images = [];

    /**
     * @param string $locale
     * @return string
     */
    public static function word(string $locale): string
    {
        $words = self::getDictionary($locale)[self::DictionaryWords];

        return $words[array_rand($words)];
    }

    /**
     * @return string
     */
    public static function letter(): string
    {
        return self::AlphabetCharacters[array_rand(self::AlphabetCharacters)];
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function firstName(string $locale): string
    {
        $words = self::getDictionary($locale)[self::DictionaryFirstNames];

        return $words[array_rand($words)];
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function lastName(string $locale): string
    {
        $words = self::getDictionary($locale)[self::DictionaryLastNames];

        return $words[array_rand($words)];
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function fullName(string $locale): string
    {
        return self::firstName($locale) . ' ' . self::lastName($locale);
    }

    /**
     * @return bool
     */
    public static function boolean(): bool
    {
        return self::randomInt(0, 1) === 1;
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    public static function integer(int $min = PHP_INT_MIN, int $max = PHP_INT_MAX): int
    {
        return self::randomInt($min, $max);
    }

    /**
     * @param int $min
     * @param int $max
     * @param int $decimals
     * @return int
     */
    public static function float(int $min = PHP_INT_MIN, int $max = PHP_INT_MAX - 1, int $decimals = 2): int
    {
        $pow = 10 ^ $decimals;

        return self::randomInt($min, $max) + self::randomInt(0, $pow) / $pow;
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function domain(string $locale): string
    {
        return Strings::webalize(self::word($locale)) . '.fake';
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function password(string $locale): string
    {
        return self::word($locale) . '-' . self::special(self::randomInt(0, 4)) . '-' . self::randomInt(1000, 9999);
    }

    /**
     * @param string $locale
     * @param bool $useScheme
     * @param string $subdomain
     * @param bool $usePath
     * @param bool $useQuery
     * @return string
     */
    public static function url(string $locale, bool $useScheme = true, string $subdomain = 'www', bool $usePath = true, bool $useQuery = true): string
    {
        $url = [];
        if ($useScheme) {
            $url[] = 'https://';
        }

        if ($subdomain) {
            $url[] = $subdomain . '.';
        }

        $url[] = self::domain($locale);

        if ($usePath) {
            $url[] = '/' . Strings::webalize(self::word($locale)) . '/';
        }

        if ($useQuery) {
            $url[] = '?' . http_build_query([
                Strings::webalize(self::word($locale)) => self::word($locale),
                Strings::webalize(self::word($locale)) => self::integer(),
                Strings::webalize(self::word($locale)) => self::float(),
                Strings::webalize(self::word($locale)) => self::boolean(),
            ]);
        }

        return implode($url);
    }

    /**
     * @param int $length
     * @return string
     */
    public static function special(int $length): string
    {
        $word = '';
        for ($i = 0; $i < $length; $i++) {
            $word .= self::SpecialCharacters[array_rand(self::SpecialCharacters)];
        }

        return $word;
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function email(string $locale): string
    {
        return Strings::webalize(self::word($locale)) . '@' . self::domain($locale);
    }

    /**
     * @return string
     */
    public static function phone(): string
    {
        return '00420555' . self::randomInt(100000, 999999);
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function company(string $locale): string
    {
        $company = [];
        for ($i = 0; $i < self::integer(1, 3); $i++) {
            $company = self::word($locale);
        }

        $company = ucfirst(implode(' ', $company));
        $company .= ', ';

        for ($i = 0; $i < self::integer(2, 4); $i++) {
            $company .= self::letter() . '.';
        }

        return $company;
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function street(string $locale): string
    {
        $street = [];
        for ($i = 0; $i < self::integer(1, 3); $i++) {
            $street = self::word($locale);
        }

        return ucfirst(implode(' ', $street));
    }

    /**
     * @param string $locale
     * @return string
     */
    public static function city(string $locale): string
    {
        $city = [];
        for ($i = 0; $i < self::integer(1, 3); $i++) {
            $city = self::word($locale);
        }

        return ucfirst(implode(' ', $city));
    }

    /**
     * @param string $locale
     * @param int $wordsCount
     * @param bool $useDots
     * @return string
     */
    public static function sentence(string $locale, int $wordsCount, bool $useDots = true): string
    {
        $words = self::getDictionary($locale)[self::DictionaryWords];

        $sentence = [];
        $upper = true;
        for ($i = 0; $i < $wordsCount; $i++) {
            $word = $words[array_rand($words)];
            if ($upper) {
                $word = ucfirst($word);
                $upper = false;
            }

            if ($useDots && ($i === ($wordsCount - 1) || self::randomInt(0, 5) === 0)) {
                $word .= '.';
                $upper = true;
            }

            $sentence[] = $word;
        }

        return implode(' ', $sentence);
    }

    /**
     * @param string|null $library
     * @return string
     */
    public static function image(string $library = null): string
    {
        $images = self::getImages($library);

        return $images[array_rand($images)];
    }

    /**
     * @param string|null $locale
     * @return array
     */
    private static function getDictionary(?string $locale = null): array
    {
        $locale = $locale ?? 'default';

        if (! array_key_exists($locale, self::$dictionaries)) {
            self::$dictionaries[$locale] = $locale === 'default'
                ? include __DIR__ . DIRECTORY_SEPARATOR . 'Dictionaries' . DIRECTORY_SEPARATOR . 'default.php'
                : array_merge(
                    include __DIR__ . DIRECTORY_SEPARATOR . 'Dictionaries' . DIRECTORY_SEPARATOR . 'default.php',
                    include __DIR__ . DIRECTORY_SEPARATOR . 'Dictionaries' . DIRECTORY_SEPARATOR . $locale . '.php',
                );
        }

        return self::$dictionaries[$locale];
    }

    /**
     * @param string|null $library
     * @return array
     */
    private static function getImages(?string $library = null): array
    {
        $library = $library ?? __DIR__ . '/Images/unsplash.php';

        if (! array_key_exists($library, self::$images)) {
            self::$images[$library] = require $library;
        }

        return self::$images[$library];
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    private static function randomInt(int $min, int $max): int
    {
        try {
            return random_int($min, $max);
        } catch (Exception) {
            return $min;
        }
    }
}
