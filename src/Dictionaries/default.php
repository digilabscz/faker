<?php declare(strict_types=1);

use Digilabscz\Faker\Generator;

return [
    Generator::DictionaryWords => [
        'Lorem',
        'Ipsum',
    ],
    Generator::DictionaryFirstNames => [
        'Lorem'
    ],
    Generator::DictionaryLastNames => [
        'Ipsum'
    ],
];
